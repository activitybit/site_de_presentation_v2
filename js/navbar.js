const COLLAPSE_POINT = 992;
const PRIMARY_COLOR = "#F9F3CC";
const DARK_COLOR = "#373741";
const BACKGROUND_COLOR = "#F5F5F5";
const IS_TOUCH_DEVICE = () => {
  return window.matchMedia("(pointer: coarse)").matches;
};

//scrool tout en haut de la page au chargement
// document.documentElement.scrollTop = 0;

/**
 * Révéler ou cacher la navigation du menu "hamburger".
 * @param {Event} event
 */
function toggleNavbar(event) {
  let isCollapse = window.innerWidth < COLLAPSE_POINT;
  if (isCollapse) {
    let navbar = document.getElementById("navbar");
    navbar.classList.toggle("hiddenNavbar");
    navbar.classList.toggle("visibleNavbar");
    hideExperience();
    animeHamburgerBtn();
    event.stopPropagation();
  }
}

/**
 * Cacher la navigation du menu "hamburger" si il est ouvert.
 */
function hideNavbar() {
  let navbar = document.getElementById("navbar");
  let isCollapse = window.innerWidth < COLLAPSE_POINT;
  let isVisibleNavbar = navbar.classList.contains("visibleNavbar");
  if (isCollapse && isVisibleNavbar) {
    navbar.classList.add("hiddenNavbar");
    navbar.classList.remove("visibleNavbar");
    animeHamburgerBtn();
  }
}

function animeHamburgerBtn() {
  let hamburgerBtn = document.getElementById("hamburgerBtn");
  hamburgerBtn.classList.toggle("active");
  hamburgerBtn.classList.toggle("not-active");
}

/**
 * Révéler ou cacher le sous-menu "expériences".
 */
function toggleExperience(event) {
  let experiencesContent = document.querySelector(".experiencesContent");

  if (experiencesContent.classList.contains("hiddenElement")) {
    experiencesContent.classList.remove("hiddenElement");
    experiencesContent.classList.add("visibleElement");
    rotatingUp();
  } else {
    experiencesContent.classList.remove("visibleElement");
    experiencesContent.classList.add("hiddenElement");
    rotatingDown();
  }
  event.stopPropagation();
}

/**
 * Cacher le sous-menu "expériences" si il est ouvert.
 */
function hideExperience() {
  let experiencesContent = document.querySelector(".experiencesContent");
  if (experiencesContent.classList.contains("visibleElement")) {
    experiencesContent.classList.toggle("hiddenElement", true);
    experiencesContent.classList.toggle("visibleElement", false);
    rotatingDown();
  }
}

/**
 * Révéler le sous-menu "expérience" si il est fermé.
 */
function displayExperience() {
  let experiencesContent = document.querySelector(".experiencesContent");
  experiencesContent.classList.toggle("hiddenElement", false);
  experiencesContent.classList.toggle("visibleElement", true);
}

/**
 * dropdown icon du sous-menu "expériences" vers le haut
 */
function rotatingUp() {
  let dropDown = document.getElementById("dropDownId");
  dropDown.classList.toggle("iconDown", false);
  dropDown.classList.toggle("iconUp", true);
}

/**
 * dropdown icon du sous-menu "expériences" vers le bas
 */
function rotatingDown() {
  let dropDown = document.getElementById("dropDownId");
  dropDown.classList.toggle("iconUp", false);
  dropDown.classList.toggle("iconDown", true);
}

/**
 * dropdown icon du sous-menu "expériences" de la couleur primary.
 */
function colorIconUp() {
  let polygon = document.getElementById("dropDownColor");
  polygon.setAttribute("fill", PRIMARY_COLOR);
}

/**
 * dropdown icon du sous-menu "expériences" de la couleur dark.
 */
function colorIconDown() {
  let polygon = document.getElementById("dropDownColor");
  polygon.setAttribute("fill", DARK_COLOR);
}

/**
 * Définit la couleur de l'icon dropdown,
 * en fonction de la version mobile ou desktop.
 */
function initDropdownIconColor() {
  let isCollapse = window.innerWidth < COLLAPSE_POINT;
  let polygon = document.getElementById("dropDownColor");
  let color = polygon.getAttribute("fill");
  if (isCollapse && color != BACKGROUND_COLOR) {
    polygon.setAttribute("fill", BACKGROUND_COLOR);
  } else if (!isCollapse && color != DARK_COLOR) {
    polygon.setAttribute("fill", DARK_COLOR);
  }
}

function handleNavAnchorsOnMouseEnter() {
  let menu = document.getElementById("idMenu");
  let anchors = menu.querySelectorAll("a");

  for (let i = 0; i < anchors.length; i++) {
    anchors[i].addEventListener("mouseenter", function () {
      let isCollapse = window.innerWidth < COLLAPSE_POINT;
      if (!isCollapse && !IS_TOUCH_DEVICE()) {
        anchors[i].classList.add("hover");
      }
    });
  }
}

function handleNavAnchorsOnMouseLeave() {
  let menu = document.getElementById("idMenu");
  let anchors = menu.querySelectorAll("a");

  for (let i = 0; i < anchors.length; i++) {
    anchors[i].addEventListener("mouseleave", function () {
      let isCollapse = window.innerWidth < COLLAPSE_POINT;
      if (!isCollapse && !IS_TOUCH_DEVICE()) {
        anchors[i].classList.remove("hover");
      }
    });
  }
}

/**
 * Révéler le sous-menu "expériences" au passage de la souris;
 */
function handleExperienceOnMouseEnter() {
  let experiencesContainer = document.querySelector(".experiencesContainer");

  experiencesContainer.addEventListener("mouseenter", function () {
    let experiencesTab = document.querySelector(".experiencesTab");
    let isCollapse = window.innerWidth < COLLAPSE_POINT;

    if (!isCollapse && !IS_TOUCH_DEVICE()) {
      experiencesTab.classList.add("hover");
      displayExperience();
      rotatingUp();
      colorIconUp();
    }
  });
}

/**
 * Cacher le sous-menu "expériences" au passage de la souris;
 */
function handleExperienceOnMouseLeave() {
  let experiencesContainer = document.querySelector(".experiencesContainer");
  experiencesContainer.addEventListener("mouseleave", function () {
    let experiencesTab = document.querySelector(".experiencesTab");
    let isCollapse = window.innerWidth < COLLAPSE_POINT;
    if (!isCollapse && !IS_TOUCH_DEVICE()) {
      experiencesTab.classList.remove("hover");
      hideExperience();
      rotatingDown();
      colorIconDown();
    }
  });
}

if (!IS_TOUCH_DEVICE()) {
  handleNavAnchorsOnMouseEnter();
  handleNavAnchorsOnMouseLeave();
  handleExperienceOnMouseEnter();
  handleExperienceOnMouseLeave();
}

/**
 * Cache la navbar du menu hamburger et le sous-menu "expériences",
 * si la fenêtre est redimenssionnée.
 */
function handleResize() {
  let navbar = document.getElementById("navbar");
  let isCollapse = window.innerWidth < COLLAPSE_POINT;
  initDropdownIconColor();
  hideExperience();
  if (isCollapse) {
    navbar.classList.add("hiddenNavbar");
    hideNavbar();
  } else {
    navbar.classList.remove("hiddenNavbar");
  }
}

handleResize();
window.addEventListener("resize", handleResize);

/**
 * Ferme le sous-menu "expériences" si l'utilisateur clique en dehors de ce dernier.
 */
document.addEventListener("click", function () {
  let experiencesContent = document.querySelector(".experiencesContent");
  let isHiddenExperience =
    experiencesContent.classList.contains("hiddenElement");
  let isCollapse = window.innerWidth < COLLAPSE_POINT;

  if (!isCollapse && !isHiddenExperience) {
    hideExperience();
  }
});
