const START_BTN_TEXT = "code source";
const TRAP_BTN_TEXT = "Nouvelle Chance ?";
const RESET_BTN_TEXT = "On fait reset ?";
const LOADIN_BTN_TEXT = "computing";
const ERROR_TEXT = "[error]";
const SUCCESS_TEXT = "[success]";
const SUCCESS_COLOR = "#00fa9a";
const ERROR_COLOR = "#FF6347";
const MESSAGE_1 = "la chance ne donne pas, elle ne fait que prêter.";
const MESSAGE_2 = "clique aux bons endroits, et victorieux tu seras !";
const VICTORY_PATH = "Tu es à un clic de la victoire !";
const SOURCE_CODE_LINK_1 = "https://gitlab.com/sircornflex/opentrivia";
const SOURCE_CODE_LINK_2 =
  "https://gitlab.com/sircornflex/site_de_presentation_v2";

function injectGameBtnText() {
  document.getElementById("gameBtnText").textContent =
    START_BTN_TEXT.toUpperCase();
}

function displayComputing() {
  let gameBtn = document.getElementById("gameBtn");

  //désactivation de l'event listener, afin de ne pas
  //relancer le début du jeu lors du prochain clic sur le bouton
  gameBtn.removeEventListener("click", startGame);

  //injecte la valeur de "LOADIN_BTN_TEXT" dans le bouton
  document.getElementById("gameBtnText").textContent =
    LOADIN_BTN_TEXT.toUpperCase();

  //Cursor progress
  gameBtn.style.cursor = "progress";

  //Gif Loading visible
  document.querySelector(".loadingGifContainer").style.display = "block";
}

function rotating() {
  let imgWin = document.getElementById("imgWin");
  imgWin.classList.add("rotating");
  imgWin.style.opacity = "0";

  let imgFail = document.getElementById("imgFail");
  imgFail.classList.add("rotating");
  imgFail.style.opacity = "1";
}

function displayWiseMessage() {
  let gameBtn = document.getElementById("gameBtn");

  gameBtn.style.cursor = "pointer";

  //injecte la valeur de "trapBtnText" dans le bouton
  document.getElementById("gameBtnText").textContent =
    TRAP_BTN_TEXT.toUpperCase();

  //Suppression du gif loading
  document.querySelector(".loadingGifContainer").style.display = "none";

  //injection du texte dans le <p> wiseMessage1
  let wiseMessage1 = document.getElementById("wiseMessage1");
  wiseMessage1.innerText = MESSAGE_1.toUpperCase();

  //injection du texte dans le <p> wiseMessage2
  let wiseMessage2 = document.getElementById("wiseMessage2");
  wiseMessage2.innerText = MESSAGE_2.toUpperCase();

  //La sélection de tous les h3 de la class projetsInfoPrezContainer renvoie une liste de h3
  let h3List = document.querySelectorAll(".projetsInfoPrezContainer h3");

  //hydratation des elements h3 avec une foreach
  h3List.forEach((element) => (element.innerText = ERROR_TEXT.toUpperCase()));

  // ajout d'un filtre CSS dans la classe "prezFigure"
  let prezFigure = document.getElementsByClassName("prezFigure");

  for (let i = 0; i < prezFigure.length; i++) {
    prezFigure[i].style.filter = "brightness(30%)";
  }
}

function loadResetListener() {
  let gameBtn = document.getElementById("gameBtn");
  gameBtn.addEventListener("click", reset);
}

function reset() {
  let gameBtn = document.getElementById("gameBtn");
  gameBtn.removeEventListener("click", reset);

  //suppression des boutons
  let btnCollection = document.getElementsByClassName("btn_green");
  console.log(btnCollection);
  if (btnCollection.length) {
    while (btnCollection.length > 0) {
      btnCollection[0].remove();
    }
  }

  //suppression du texte
  let messageCollection = document.getElementsByClassName("message");
  for (let i = 0; i < messageCollection.length; i++) {
    messageCollection[i].querySelector("h3").innerText = "";
    messageCollection[i].querySelector("h3").removeAttribute("style");
    messageCollection[i].querySelector("p").textContent = "";
    messageCollection[i].querySelector("a").href = "";
  }

  //écrit le message d'origine sur le bouton (début du jeu)
  document.getElementById("gameBtnText").textContent =
    START_BTN_TEXT.toUpperCase();

  //retour de l'image win
  let imgWin = document.getElementById("imgWin");
  imgWin.style.opacity = "1";

  let imgFail = document.getElementById("imgFail");
  imgFail.style.opacity = "0";

  //suppression de la class rotating
  imgWin.classList.remove("rotating");
  imgFail.classList.remove("rotating");

  //supression cursor pointer
  let message1Container = document.getElementById("message1Container");
  let message2Container = document.getElementById("message2Container");
  message1Container.removeAttribute("style");
  message2Container.removeAttribute("style");

  //suppression eventlistener
  message1Container.removeEventListener("click", getSuccessMessage1);
  message2Container.removeEventListener("click", getSuccessMessage2);

  //suppression du filtre css dans la classe prezFigure
  let prezFigure = document.getElementsByClassName("prezFigure");
  for (let i = 0; i < prezFigure.length; i++) {
    prezFigure[i].style.removeProperty("filter");
  }

  //ajout du listener qui déclenche le début du jeu
  gameBtn.addEventListener("click", startGame);
}

function displaySuccessAll() {
  let message1Container = document.getElementById("message1Container");
  let message2Container = document.getElementById("message2Container");

  //suppression des listener afin d'éviter de repasser dans cette fonction
  message1Container.removeEventListener("click", getSuccessMessage1);
  message2Container.removeEventListener("click", getSuccessMessage2);

  let tabPrez = [message1Container, message2Container];

  //Affichage des propriétés Success
  for (i = 0; i < tabPrez.length; i++) {
    tabPrez[i].querySelector("h3").innerText = SUCCESS_TEXT.toUpperCase();
    tabPrez[i].querySelector("h3").style.color = SUCCESS_COLOR;
    tabPrez[i].removeAttribute("style");
    tabPrez[i].querySelector("p").innerText = "";
    tabPrez[i].classList.remove("messageCenterVerticaly");
  }

  //injection dans les ancres des lien href
  let anchor1 = document.getElementById("sourceCode1");
  let anchor2 = document.getElementById("sourceCode2");
  anchor1.href = SOURCE_CODE_LINK_1;
  anchor2.href = SOURCE_CODE_LINK_2;

  //injection dans les ancres d'un bouton pour accéder au code source
  let codeSourceBtn = document.createElement("button");
  codeSourceBtn.setAttribute("type", "button");
  codeSourceBtn.classList.add("btn_green");
  codeSourceBtn.textContent = START_BTN_TEXT.toUpperCase();
  anchor1.appendChild(codeSourceBtn.cloneNode(true));
  anchor2.appendChild(codeSourceBtn.cloneNode(true));

  let anchorApk = document.getElementById("apk");
  let apkBtn = document.createElement("button");
  apkBtn.setAttribute("type", "button");
  apkBtn.classList.add("btn_green");
  apkBtn.textContent = "APK";
  anchorApk.appendChild(apkBtn);

  //injection du texte reset dans le bouton
  document.getElementById("gameBtnText").textContent =
    RESET_BTN_TEXT.toUpperCase();

  //suppression de la class rotating
  imgWin.classList.remove("rotating");
  imgFail.classList.remove("rotating");

  //retour de l'image win
  document.getElementById("imgWin").style.opacity = "1";
  document.getElementById("imgFail").style.opacity = "0";

  let btnCollection = document.getElementsByClassName("btn_green");
  console.log(btnCollection);
}

function getSuccessMessage1() {
  let message1Container = document.getElementById("message1Container");
  let message2Container = document.getElementById("message2Container");

  if (
    message2Container.querySelector("h3").innerText == ERROR_TEXT.toUpperCase()
  ) {
    message1Container.querySelector("p").innerText = "";
    let h3 = message1Container.querySelector("h3");
    h3.innerText = VICTORY_PATH;
    h3.style.color = "#FFFFFF";
    message1Container.classList.add("messageCenterVerticaly");

    //enlever le cursor pointer
    message1Container.removeAttribute("style");
  } else {
    displaySuccessAll();
  }
}

function getSuccessMessage2() {
  let message1Container = document.getElementById("message1Container");
  let message2Container = document.getElementById("message2Container");

  if (
    message1Container.querySelector("h3").innerText == ERROR_TEXT.toUpperCase()
  ) {
    message2Container.querySelector("p").innerText = "";
    let h3 = message2Container.querySelector("h3");
    h3.innerText = VICTORY_PATH;
    h3.style.color = "#FFFFFF";
    message2Container.classList.add("messageCenterVerticaly");
    message2Container.removeAttribute("style");
  } else {
    displaySuccessAll();
  }
}

function success() {
  console.log("getSuccess");
  let message1Container = document.getElementById("message1Container");
  let message2Container = document.getElementById("message2Container");

  //Mettre un cursor Pointer sur "message1Container" et "message2Container"
  message1Container.onmouseover = message1Container.style.cursor = "pointer";
  message2Container.onmouseover = message2Container.style.cursor = "pointer";

  message1Container.addEventListener("click", getSuccessMessage1);
  message2Container.addEventListener("click", getSuccessMessage2);
}

function startGame() {
  displayComputing();
  setTimeout("rotating()", 1500);
  setTimeout("displayWiseMessage()", 5500);
  setTimeout("success()", 5500);
  setTimeout("loadResetListener()", 5500);
}

//texte du bouton de démarrage du jeu
injectGameBtnText();

//écouteur sur le bouton afin de commencer le jeu lors du clic
document.getElementById("gameBtn").addEventListener("click", startGame);
