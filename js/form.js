const MAX_LENGTH_MESSAGE = 500;
const MAX_LENGTH_EMAIL = 30;
const MAX_LENGTH_NAME = 20;
const MIN_LENGTH_NAME = 3;
const EMAIL_REGEX =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const FORM_FIELDS = {
  firstName: document.getElementById("fFirstName"),
  lastName: document.getElementById("fLastName"),
  email: document.getElementById("fEmail"),
  message: document.getElementById("fMessage"),
};

const ERROR_FIELDS = {
  firstName: document.getElementById("errorFirstName"),
  lastName: document.getElementById("errorLastName"),
  email: document.getElementById("errorEmail"),
  message: document.getElementById("errorMessage"),
};

////////validateurs////////
function requiredValidator(inputField, errorField) {
  if (inputField.value.trim()) {
    errorField.textContent = "";
    return true;
  } else {
    errorField.textContent = "Ce champ est requis";
    return false;
  }
}

function emailValidator(inputField, errorField) {
  if (!inputField.value.trim()) {
    return true;
  }

  if (EMAIL_REGEX.test(inputField.value)) {
    errorField.textContent = "";
    return true;
  } else {
    errorField.textContent = "Email non valide";
    return false;
  }
}

function minLengthValidator(inputField, minLength, errorField) {
  if (!inputField.value.trim()) {
    return true;
  }

  if (inputField.value.trim().length >= minLength) {
    errorField.textContent = "";
    return true;
  } else {
    errorField.textContent = minLength + " caractères minimum";
    return false;
  }
}

function maxLengthValidator(inputField, maxLength, errorField) {
  if (!inputField.value.trim()) {
    return true;
  }

  if (inputField.value.trim().length <= maxLength) {
    errorField.textContent = "";
    return true;
  } else {
    errorField.textContent = maxLength + " caractères maximum";
    return false;
  }
}

////////fonctions de validation////////
//Elles intègrent une condition propre à chaque champ,
//afin de ne pas déclencher plusieurs validations en même temps.
function validateFirstNameOnInput() {
  if (
    FORM_FIELDS.firstName.value.trim() &&
    FORM_FIELDS.firstName.value.trim().length > MAX_LENGTH_NAME
  ) {
    return maxLengthValidator(
      FORM_FIELDS.firstName,
      MAX_LENGTH_NAME,
      ERROR_FIELDS.firstName
    );
  }
  return requiredValidator(FORM_FIELDS.firstName, ERROR_FIELDS.firstName);
}

function validateFirstNameOnBlur() {
  if (
    FORM_FIELDS.firstName.value.trim() &&
    FORM_FIELDS.firstName.value.trim().length < MIN_LENGTH_NAME
  ) {
    return minLengthValidator(
      FORM_FIELDS.firstName,
      MIN_LENGTH_NAME,
      ERROR_FIELDS.firstName
    );
  }
  return true;
}

function validateLastNameOnInput() {
  if (
    FORM_FIELDS.lastName.value.trim() &&
    FORM_FIELDS.lastName.value.trim().length > MAX_LENGTH_NAME
  ) {
    return maxLengthValidator(
      FORM_FIELDS.lastName,
      MAX_LENGTH_NAME,
      ERROR_FIELDS.lastName
    );
  }
  return requiredValidator(FORM_FIELDS.lastName, ERROR_FIELDS.lastName);
}

function validateLastNameOnBlur() {
  if (
    FORM_FIELDS.lastName.value.trim() &&
    FORM_FIELDS.lastName.value.trim().length < MIN_LENGTH_NAME
  ) {
    return minLengthValidator(
      FORM_FIELDS.lastName,
      MIN_LENGTH_NAME,
      ERROR_FIELDS.lastName
    );
  }
  return true;
}

function validateEmailOnInput() {
  if (
    FORM_FIELDS.email.value.trim() &&
    FORM_FIELDS.email.value.trim().length > MAX_LENGTH_EMAIL
  ) {
    return maxLengthValidator(
      FORM_FIELDS.email,
      MAX_LENGTH_EMAIL,
      ERROR_FIELDS.email
    );
  }
  return requiredValidator(FORM_FIELDS.email, ERROR_FIELDS.email);
}

function validateEmailOnBlur() {
  //si maxLengthValidator est actif, pas de emailValidator
  if (
    FORM_FIELDS.email.value.trim() &&
    FORM_FIELDS.email.value.trim().length > MAX_LENGTH_EMAIL
  ) {
    return true;
  }
  return emailValidator(FORM_FIELDS.email, ERROR_FIELDS.email);
}

function validateMessageOnInput() {
  if (
    FORM_FIELDS.message.value.trim() &&
    FORM_FIELDS.message.value.trim().length > MAX_LENGTH_MESSAGE
  ) {
    return maxLengthValidator(
      FORM_FIELDS.message,
      MAX_LENGTH_MESSAGE,
      ERROR_FIELDS.message
    );
  }
  return requiredValidator(FORM_FIELDS.message, ERROR_FIELDS.message);
}

function isFormValid() {
  const validations = [];
  validations.push(validateFirstNameOnInput());
  validations.push(validateFirstNameOnBlur());
  validations.push(validateLastNameOnInput());
  validations.push(validateLastNameOnBlur());
  validations.push(validateEmailOnInput());
  validations.push(validateEmailOnBlur());
  validations.push(validateMessageOnInput());

  // Vérifiez si toutes les validations sont valides
  const isValid = validations.every((result) => result === true);
  return isValid;
}

////////fonctions diverses////////

/**
 * Stylise un champ de formulaire pendant l'entrée, en fonction d'une erreur de validation, ou non.
 * Les constantes ERROR_COLOR et SUCCESS_COLOR sont déjà déclarées dans le fichier jeu.js
 * @param {HTMLElement} inputField
 * @param {innerText} errorField
 */
function setFieldStyleOnInput(inputField, errorField) {
  if (errorField) {
    inputField.style.borderColor = ERROR_COLOR;
  } else {
    inputField.style.borderColor = "#5468ff";
  }
}

/**
 * Stylise un champ de formulaire à sa sortie, en fonction d'une erreur de validation, ou non.
 * Les constantes ERROR_COLOR et SUCCESS_COLOR sont déjà déclarées dans le fichier jeu.js
 * @param {HTMLElement} inputField
 * @param {innerText} errorField
 * @param {boolean} isUserTyping
 */
function setFieldStyleOnBlur(inputField, errorField, isUserTyping) {
  //Si l'utilisateur n'a rien tapé, la fonction ne se s'exécute pas.
  if (!isUserTyping) {
    return;
  }

  if (errorField) {
    inputField.style.borderColor = ERROR_COLOR;
  } else {
    inputField.style.borderColor = SUCCESS_COLOR;
  }
}

function resetForm() {
  let fields = [];
  fields.push(FORM_FIELDS.firstName);
  fields.push(FORM_FIELDS.lastName);
  fields.push(FORM_FIELDS.email);
  fields.push(FORM_FIELDS.message);

  for (let i = 0; i < fields.length; i++) {
    fields[i].style.borderColor = "rgba(0, 0, 0, 0.37)";
  }

  document.getElementById("form").reset();
}

////////écouteurs////////
let isUserTyping = false;

FORM_FIELDS.firstName.addEventListener("input", function () {
  isUserTyping = true;
  validateFirstNameOnInput();

  let firstNameField = FORM_FIELDS.firstName;
  let errorInnerText = ERROR_FIELDS.firstName.innerText;
  setFieldStyleOnInput(firstNameField, errorInnerText);
});

FORM_FIELDS.firstName.addEventListener("blur", function () {
  validateFirstNameOnBlur();

  let firstNameField = FORM_FIELDS.firstName;
  let errorInnerText = ERROR_FIELDS.firstName.innerText;
  setFieldStyleOnBlur(firstNameField, errorInnerText, isUserTyping);

  isUserTyping = false;
});

FORM_FIELDS.lastName.addEventListener("input", function () {
  isUserTyping = true;
  validateLastNameOnInput();

  let lastNameField = FORM_FIELDS.lastName;
  let errorInnerText = ERROR_FIELDS.lastName.innerText;
  setFieldStyleOnInput(lastNameField, errorInnerText);
});

FORM_FIELDS.lastName.addEventListener("blur", function () {
  validateLastNameOnBlur();

  let lastNameField = FORM_FIELDS.lastName;
  let errorInnerText = ERROR_FIELDS.lastName.innerText;
  setFieldStyleOnBlur(lastNameField, errorInnerText, isUserTyping);

  isUserTyping = false;
});

FORM_FIELDS.email.addEventListener("input", function () {
  isUserTyping = true;
  validateEmailOnInput();

  let emailField = FORM_FIELDS.email;
  let errorInnerText = ERROR_FIELDS.email.innerText;
  setFieldStyleOnInput(emailField, errorInnerText);
});

FORM_FIELDS.email.addEventListener("blur", function () {
  validateEmailOnBlur();

  let emailField = FORM_FIELDS.email;
  let errorInnerText = ERROR_FIELDS.email.innerText;
  setFieldStyleOnBlur(emailField, errorInnerText, isUserTyping);

  isUserTyping = false;
});

FORM_FIELDS.message.addEventListener("input", function () {
  isUserTyping = true;
  validateMessageOnInput();

  let messageField = FORM_FIELDS.message;
  let errorInnerText = ERROR_FIELDS.message.innerText;
  setFieldStyleOnInput(messageField, errorInnerText);
});

FORM_FIELDS.message.addEventListener("blur", function () {
  let messageField = FORM_FIELDS.message;
  let errorInnerText = ERROR_FIELDS.message.innerText;
  setFieldStyleOnBlur(messageField, errorInnerText, isUserTyping);

  isUserTyping = false;
});

// function sendEmail() {
//   console.log("---> senEmail");
//   if (isFormValid()) {
//     console.log("---> senEmail + form valid");
//     resetForm();
//   }
// }

async function sendEmail() {
  if (!isFormValid()) {
    // alert("invalid form");
    return;
  }

  const FORM_DATA = {
    firstName: document.getElementById("fFirstName").value,
    lastName: document.getElementById("fLastName").value,
    email: document.getElementById("fEmail").value,
    message: document.getElementById("fMessage").value,
  };

  const serviceId = "service_t4lbciz";
  const templateId = "template_3lkerrx";

  try {
    console.log("before");
    await emailjs.send(serviceId, templateId, FORM_DATA);
    alert("message send");
    console.log("after");
  } catch (error) {
    console.log(error);
  }
}
